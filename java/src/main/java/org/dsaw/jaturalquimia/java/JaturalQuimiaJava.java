package org.dsaw.jaturalquimia.java;

import org.dsaw.jaturalquimia.core.JaturalQuimia;
import playn.core.PlayN;
import playn.java.JavaPlatform;

public class JaturalQuimiaJava {

    public static void main(String[] args) {
        JavaPlatform.Config config = new JavaPlatform.Config();
        // use config to customize the Java platform, if needed
        JavaPlatform.register(config);
        PlayN.run(new JaturalQuimia());
    }
}
