package org.dsaw.jaturalquimia.android;

import playn.android.GameActivity;
import playn.core.PlayN;

import org.dsaw.jaturalquimia.core.JaturalQuimia;

public class JaturalQuimiaActivity extends GameActivity {

  @Override
  public void main(){
    PlayN.run(new JaturalQuimia());
  }
}
