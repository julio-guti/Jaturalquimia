package org.dsaw.jaturalquimia.core;
public enum Direction {
    UP, DOWN, LEFT, RIGHT;
}
