package org.dsaw.jaturalquimia.core;

import playn.core.GroupLayer;
import playn.core.Image;
import playn.core.ImageLayer;
import playn.core.Layer;

import static playn.core.PlayN.assets;
import static playn.core.PlayN.graphics;

public class Board {

    private final int _width;
    private final int _height;
    private final GroupLayer _layer;
    private final Element[][] _board;

    public Board(int width, int height) {
        _width = width;
        _height = height;

        _layer = graphics().createGroupLayer(_width * Element.SIZE, _height * Element.SIZE);
        _layer.setTranslation(0, Element.SIZE * 3); //TODO

        _board = new Element[_width][_height];

        Image bgImage = assets().getImage("images/bg.png");
        ImageLayer bgLayer = graphics().createImageLayer(bgImage);
        _layer.add(bgLayer);
    }

    public void add(Element element, int position) {
        int x = position * Element.SIZE;

        _layer.addAt(element.getLayer(), x, 0);
        element.setPos(x, 0);//TODO don't like this

        _board[position][0] = element;
    }

    public void update(int delta) {
        for (int i = 0; i < _width; i++) {
            for (int j = 0; j < _height - 1; j++) {
                if (_board[i][j] != null) {
                    if (j + 1 < _height && _board[i][j + 1] == null) {
                        _board[i][j].moveDown();
                        _board[i][j + 1] = _board[i][j];
                        _board[i][j] = null;
                    }
                }
            }
        }
    }

    public void paint(float alpha) {
        for (int i = 0; i < _width; i++) {
            for (int j = 0; j < _height; j++) {
                if (_board[i][j] != null) {
                    _board[i][j].paint(alpha);
                }
            }
        }
    }

    public Layer get_layer() {
        return _layer;
    }
}