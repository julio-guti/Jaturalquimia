package org.dsaw.jaturalquimia.core;

import playn.core.Keyboard;

public class KeyboardListener extends Keyboard.Adapter {

    private final CurrentElements currentElements;
    private final Board board;

    public KeyboardListener(CurrentElements currentElements, Board board) {
        this.currentElements = currentElements;
        this.board = board;
    }

    @Override
    public void onKeyDown(Keyboard.Event event) {
        switch (event.key()) {
            case LEFT:
                currentElements.moveToLeft();
                break;
            case RIGHT:
                currentElements.moveToRight();
                break;
            case UP:
                currentElements.rotate();
                break;
            case DOWN:
                board.add(new Element(), currentElements.getPos1());
                board.add(new Element(), currentElements.getPos2());
                currentElements.clearElements();
                break;
            case K1:
                board.add(new Element(), 0);
                break;
            case K2:
                board.add(new Element(), 1);
                break;
            case K3:
                board.add(new Element(), 2);
                break;
            case K4:
                board.add(new Element(), 3);
                break;
            case K5:
                board.add(new Element(), 4);
                break;
            case K6:
                board.add(new Element(), 5);
                break;
            case K7:
                board.add(new Element(), 6);
                break;
        }
    }
}
