package org.dsaw.jaturalquimia.core;

import playn.core.Image;

import java.util.Random;

import static playn.core.PlayN.assets;

public enum ElementType {
    GreenPotion("pea.png"), YellowPotion("pea.png"), OrangePotion("pea.png"), PurplePotion("pea.png"),
    Seed("pea.png"), Tree("pea.png"), Leaf("pea.png"), Apple("pea.png"),
    Bone("pea.png"), Skull("pea.png"), Carbon("pea.png"), Gold("pea.png");
    //
    private static final Random random = new Random();
    String _imagePath;
    Image _image;

    private ElementType(String imagePath) {
        _imagePath = imagePath;
    }

    public static ElementType getRandomType() {
        ElementType[] values = ElementType.values();
        return values[random.nextInt(values.length)]; //TODO
    }

    public Image getImage() {
        if (_image == null) {
            _image = assets().getImage("images/" + _imagePath);
        }
        return _image;
    }
}

