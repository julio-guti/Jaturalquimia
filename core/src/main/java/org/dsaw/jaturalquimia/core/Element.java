package org.dsaw.jaturalquimia.core;

import playn.core.ImageLayer;
import playn.core.Layer;

import static playn.core.PlayN.graphics;

public class Element {

    public static final int SIZE = 32;
    private final ElementType type;
    private final ImageLayer layer;
    private final MovableLayer movableLayer;

    public Element() {
        type = ElementType.getRandomType();
        layer = graphics().createImageLayer(type.getImage());
        layer.setWidth(SIZE);
        layer.setHeight(SIZE);
        movableLayer = new MovableLayer(layer);
    }

    public Layer getLayer() {
        return layer;
    }

    public void setPos(int x, int y) {
        movableLayer.setPos(x, y);
    }

    public void moveDown() {
        movableLayer.move(Direction.DOWN);
    }

    public void paint(float alpha) {
        movableLayer.paint(alpha);
    }
}
