package org.dsaw.jaturalquimia.core;

import playn.core.Game;
import playn.core.GroupLayer;

import static playn.core.PlayN.graphics;
import static playn.core.PlayN.keyboard;

public class JaturalQuimia extends Game.Default {

    private final CurrentElements current;
    private final KeyboardListener keyboardListener;
    private final Board board;
    private final GroupLayer _layer;

    public JaturalQuimia() {
        super(33);
        _layer = graphics().createGroupLayer(7 * Element.SIZE, 11 * Element.SIZE);
        _layer.setTranslation(170, 50);

        current = new CurrentElements();
        board = new Board(7, 8);

        keyboardListener = new KeyboardListener(current, board);
        _layer.add(current.getLayer());
        _layer.add(board.get_layer());
    }

    @Override
    public void init() {
        GroupLayer rootLayer = graphics().rootLayer();
        rootLayer.add(_layer);
        keyboard().setListener(keyboardListener);
    }

    @Override
    public void update(int delta) {
        board.update(delta);
    }

    @Override
    public void paint(float alpha) {
        alpha = Element.SIZE / 2 * alpha;
        current.paint(alpha);
        board.paint(alpha);
    }
}
