package org.dsaw.jaturalquimia.core;

import playn.core.GroupLayer;
import playn.core.Layer;

import static playn.core.PlayN.graphics;

public class CurrentElements {

    private final GroupLayer _layer;
    private final MovableLayer _movableLayer;
    private Element _e1;
    private Element _e2;
    private int _pos1;
    private int _pos2;

    public CurrentElements() {
        //   Image bgImage = assets().getImage("images/bg.png");
        //  ImageLayer bgLayer = graphics().createImageLayer(bgImage);

        _layer = graphics().createGroupLayer(Element.SIZE * 2, Element.SIZE * 2);
        // _layer.add(bgLayer);

        _layer.setTranslation(0, 0);//TODO

        _e1 = new Element();
        _e2 = new Element();

        _layer.addAt(_e1.getLayer(), 0, Element.SIZE);
        _pos1 = 0;
        _layer.addAt(_e2.getLayer(), Element.SIZE, Element.SIZE);
        _pos2 = 1;

        _movableLayer = new MovableLayer(_layer);
    }

    public void rotate() {
        //TODO
    }

    public void clearElements() {
        _e1 = new Element();
        _e2 = new Element();
        _layer.clear();
        _layer.addAt(_e1.getLayer(), 0, Element.SIZE);
        _pos1 = 0;
        _layer.addAt(_e2.getLayer(), Element.SIZE, Element.SIZE);
        _pos2 = 1;
        _movableLayer.setPos(0, 0); //TODO
        _layer.setTranslation(0, 0);  //TODO
    }

    public Layer getLayer() {
        return _layer;
    }

    public void moveToLeft() {
        _movableLayer.move(Direction.LEFT);
        _pos1--;
        _pos2--;
    }

    public void moveToRight() {
        _movableLayer.move(Direction.RIGHT);
        _pos1++;
        _pos2++;
    }

    public int getPos1() {
        return _pos1;
    }

    public int getPos2() {
        return _pos2;
    }

    public void paint(float alpha) {
        _movableLayer.paint(alpha);
    }
}