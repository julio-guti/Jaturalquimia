package org.dsaw.jaturalquimia.core;

import playn.core.Layer;

public class MovableLayer { //TODO rename???

    private final Layer _layer;
    private int posX;
    private int posY;

    public MovableLayer(Layer layer) {
        _layer = layer;
        posX = (int) layer.tx();
        posY = (int) layer.ty();
    }

    public void setPos(int x, int y) { //TODO remove?
        posX = x;
        posY = y;
    }

    public void move(Direction direction) {
        switch (direction) {
            case UP:
                posY -= Element.SIZE;
                break;
            case DOWN:
                posY += Element.SIZE;
                break;
            case LEFT:
                posX -= Element.SIZE;
                break;
            case RIGHT:
                posX += Element.SIZE;
                break;
        }
    }

    public void paint(float alpha) {
        float y = _layer.ty();
        if (y != posY) {
            _layer.setTy(getNewCoordinate(y, posY, alpha));
        }
        float x = _layer.tx();
        if (x != posX) {
            _layer.setTx(getNewCoordinate(x, posX, alpha));
        }
    }

    private float getNewCoordinate(float coordinate, float targetCoordinate, float alpha) {
        if (coordinate < targetCoordinate) {
            coordinate += alpha;
            if (coordinate > targetCoordinate) {
                coordinate = targetCoordinate;
            }
        } else {
            coordinate -= alpha;
            if (coordinate < targetCoordinate) {
                coordinate = targetCoordinate;
            }
        }
        return coordinate;
    }
}
